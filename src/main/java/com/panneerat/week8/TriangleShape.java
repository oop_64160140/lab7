package com.panneerat.week8;

public class TriangleShape {
    private String name;
    private double sidea;
    private double sideb;
    private double sidec;

    public TriangleShape(String name, double sidea, double sideb, double sidec){
        this.name = name;
        this.sidea = sidea;
        this.sideb = sideb;
        this.sidec = sidec;
    }

    public String getName(){
        return name;
    }

    public double calculatedArea(){
        double area = 0.5 * (sidea + sideb + sidec);
        return area;
    }

    public double calculatedGirth(){
        double girth = sidea + sideb + sidec;
        return girth;
    }

    public void print() {
        System.out.println(name + "=> Area : "+ calculatedArea());
        System.out.println(name + "=> Girth : "+ calculatedGirth());
    }
}
