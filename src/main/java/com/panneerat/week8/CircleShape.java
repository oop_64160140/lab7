package com.panneerat.week8;

public class CircleShape {
    private String name;
    private double radius;

    public CircleShape(String name, double radius){
        this.name = name;
        this.radius = radius;
    }

    public String getName(){
        return name;
    }

    public double calculatedArea(){
        double area = Math.PI * (radius * radius);
        return area;
    }

    public double calculatedGirth(){
        double girth = 2 * Math.PI * radius;
        return girth;
    }

    public void print() {
        System.out.println(name + "=> Area : "+ calculatedArea());
        System.out.println(name + "=> Girth : "+ calculatedGirth());
    }
}
