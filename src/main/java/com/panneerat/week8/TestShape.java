package com.panneerat.week8;

public class TestShape {
    public static void main(String[] args){
        RectangleShape rect1 = new RectangleShape("rect1", 1, 5);
        rect1.calculatedArea();
        rect1.calculatedGirth();
        rect1.print();

        RectangleShape rect2 = new RectangleShape("rect2", 5, 3);
        rect2.calculatedArea();
        rect2.calculatedGirth();
        rect2.print();

        CircleShape circle1 = new CircleShape("circle1", 1);
        circle1.calculatedArea();
        circle1.calculatedGirth();
        circle1.print();

        CircleShape circle2 = new CircleShape("circle2", 2);
        circle2.calculatedArea();
        circle2.calculatedGirth();
        circle2.print();

        TriangleShape triangle1 = new TriangleShape("triangle", 5, 5, 6);
        triangle1.calculatedArea();
        triangle1.calculatedGirth();
        triangle1.print();
    }
}
