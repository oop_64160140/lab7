package com.panneerat.week8;

public class RectangleShape {
    private String name;
    private double width;
    private double height;

    public RectangleShape(String name, double width, double height){
        this.name = name;
        this.width = width;
        this.height = height;
    }

    public String getName(){
        return name;
    }

    public double calculatedArea(){
        double area = width * height ;
        return area;
    }

    public double calculatedGirth(){
        double girth = (width + height)*2 ;
        return girth;
    }

    public void print() {
        System.out.println(name + "=> Area : "+ calculatedArea());
        System.out.println(name + "=> Girth : "+ calculatedGirth());
    }
}
